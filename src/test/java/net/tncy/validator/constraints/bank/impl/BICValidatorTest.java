package net.tncy.validator.constraints.bank.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author student
 */
public class BICValidatorTest {

    private static BICValidator validator;

    @BeforeClass
    public static void setUpClass() throws Exception {
        validator = new BICValidator();
    }

    @Test
    public void testNullAndEmptyString() throws Exception {
        isValid("");
        isValid(null);
    }

    @Test
    public void testValidBIC() throws Exception {
        isValid("DEUTDEDBDUE"); // DEUTSCHE BANK - FRANKFURT AM MAIN
        isValid("COBADEFFXXX"); // COMMERZBANK AG - Francfort-sur-le-Main
        isValid("GKCCBEBB"); // DEXIA BANK SA - Bruxelles
        isValid("PSSTFRPPNTE"); // La Banque postale - Centre financier de Nantes
        isValid("PSSTFRPPMON"); // La Banque postale - Centre financier de Montpellie
    }

    @Test
    public void testInValidBIC() throws Exception {
        isNotValid("P+STFRPPMON"); // Fails on bank code verification
        isNotValid("GKCCBEB"); // Fails on length verification
    }

    private void isValid(String bic) {
        assertTrue("Expected a valid BIC.", validator.isValid(bic, null));
    }

    private void isNotValid(String bic) {
        assertFalse("Expected an invalid BIC.", validator.isValid(bic, null));
    }
}
