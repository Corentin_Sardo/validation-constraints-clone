package net.tncy.validator.constraints.bank.impl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author student
 */
public class IBANValidatorTest {

    private static IBANValidator validator;

    @BeforeClass
    public static void setUpClass() throws Exception {
        validator = new IBANValidator();
    }

    @Test
    public void testNullAndEmptyString() throws Exception {
        isValid("");
        isValid(null);
    }

    @Test
    public void testValidIBAN() throws Exception {
        isValid("BE43068999999501"); // Valid IBAN [BE]
    }

    @Test
    public void testInvalidIBAN() throws Exception {
        isNotValid("BE43068999999509"); // Invalid IBAN [BE] fails on mod 97
        isNotValid("BE43068999999509123"); // Invalid IBAN [BE] fails on length
    }

    private void isValid(String bic) {
        assertTrue("Expected a valid IBAN.", validator.isValid(bic, null));
    }

    private void isNotValid(String bic) {
        assertFalse("Expected a invalid IBAN.", validator.isValid(bic, null));
    }
}
