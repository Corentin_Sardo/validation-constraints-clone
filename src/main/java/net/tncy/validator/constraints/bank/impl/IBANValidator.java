package net.tncy.validator.constraints.bank.impl;

import java.math.BigInteger;
import java.util.ResourceBundle;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import net.tncy.validator.constraints.bank.IBAN;

/**
 * Regex : [a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}
 * @author student
 *
 */
public class IBANValidator implements ConstraintValidator<IBAN, String> {

    public final static int IBAN_MIN_LEN = 14;
    public final static int BBAN_MIN_LEN = IBAN_MIN_LEN - 4;
    public final static int IBAN_MAX_LEN = 34;
    public final static int BBAN_MAX_LEN = IBAN_MAX_LEN - 4;
    protected final static BigInteger NINETY_SEVEN = new BigInteger("97");
    private boolean explain = false;

    @Override
    public void initialize(IBAN constraintAnnotation) {
        explain = constraintAnnotation.explain();
    }

    @Override
    public boolean isValid(String accountNumber, ConstraintValidatorContext constraintContext) {
        if (explain) {
            constraintContext.disableDefaultConstraintViolation();
        }
        boolean isValid = false;
        if (accountNumber == null || accountNumber.trim().isEmpty()) {
            isValid = true;
        } else {
            int len = accountNumber.length();
            if (len >= IBAN_MIN_LEN && len <= IBAN_MAX_LEN) {
                String countryCode = accountNumber.substring(0, 2);
                int expectedBBANLen = IBANValidator.getBBANLen(countryCode);
                if (expectedBBANLen >= BBAN_MIN_LEN && expectedBBANLen <= BBAN_MAX_LEN) {
                    @SuppressWarnings("unused")
                    // Not involved in the validity check
                    int checkDigits = Integer.parseInt(accountNumber.substring(2, 4));
                    String basicBAN = accountNumber.substring(4);
                    if (basicBAN.length() == expectedBBANLen) {
                        String numericStr = accountNumber.substring(4) + accountNumber.substring(0, 4);
                        StringBuilder buffer = new StringBuilder();
                        char[] chars = numericStr.toCharArray();
                        for (char c : chars) {
                            if (Character.isDigit(c)) {
                                buffer.append(c);
                            } else if (Character.isUpperCase(c)) {
                                int substitute = ((int) c) - FIRST_CHAR_CODE + 10;
                                buffer.append(substitute);
                            } else {
                                if (explain) {
                                    constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.IBAN.invalidCharacter}").addConstraintViolation();
                                }
                            }
                        }
                        BigInteger bigInt = new BigInteger(buffer.toString());
                        if (BigInteger.ONE.equals(bigInt.mod(NINETY_SEVEN))) {
                            isValid = true;
                        } else {
                            if (explain) {
                                constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.IBAN.mod97Failed}").addConstraintViolation();
                            }
                        }
                    } else {
                        if (explain) {
                            constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.IBAN.invalidBBANLength}").addConstraintViolation();
                        }
                    }
                } else {
                    if (explain) {
                        constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.IBAN.unsupportedCountryCode}").addConstraintViolation();
                    }
                }
            } else {
                if (explain) {
                    constraintContext.buildConstraintViolationWithTemplate("{net.tncy.validator.constraints.bank.IBAN.invalidIBANLength}").addConstraintViolation();
                }
            }
        }
        return isValid;
    }
    private final static int FIRST_CHAR_CODE = (int) 'A';

    private static int getBBANLen(String countryCode) {
        ResourceBundle resources = ResourceBundle.getBundle("net.tncy.validator.constraints.bank.impl.IBANResourceBundle");
        Integer ibanLen = (Integer) resources.getObject(countryCode);
        return (ibanLen != null ? ibanLen - 4 : 0);
    }
}
