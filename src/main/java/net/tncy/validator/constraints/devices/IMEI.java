package net.tncy.validator.constraints.devices;

import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import java.lang.annotation.Retention;
import static java.lang.annotation.RetentionPolicy.RUNTIME;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;
import net.tncy.validator.constraints.devices.impl.IMEIValidator;

@Target({METHOD, FIELD, ANNOTATION_TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = IMEIValidator.class)
@Documented
public @interface IMEI {

    String message() default "{net.tncy.validator.constraints.devices.IMEI}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    boolean explain() default false;
}
