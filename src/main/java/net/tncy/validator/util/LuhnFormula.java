package net.tncy.validator.util;

import java.util.Arrays;

/**
 * Formule de Luhn
 *
 * @see <a href="http://fr.wikipedia.org/wiki/Formule_de_Luhn">Formule de Luhn
 * sur Wikipedia</a>
 * @see <a href="http://planetcalc.com/2464/">Formule de Luhn sur Planetcalc</a>
 *
 * @author x-roy
 */
public class LuhnFormula {

    /**
     * Les valeurs pour les nombres de rang pair sont calculés en doublant la
     * valeur et en sommant les chiffres qui compose le résultat si celui-ci est
     * supérieur à 9 ((v*2) % 10) + ((v*2) / 10)
     */
    private final static int[] EVEN_VALUES = {0, 2, 4, 6, 8, 1, 3, 5, 7, 9};

    public static boolean isValid(int number) {
        return isValid(ValidatorUtil.toDigitArray(number));
    }

    public static boolean isValid(int[] digits) {
        int sum = computeSum(digits);
        return (sum % 10) == 0;
    }

    public static int computeChecksumDigit(int number) {
        return computeChecksumDigit(ValidatorUtil.toDigitArray(number));
    }

    public static int computeChecksumDigit(int[] digits) {
        int sum = computeSum(Arrays.copyOf(digits, digits.length + 1));
        int checksumDigit = 0;
        if (sum % 10 != 0) {
            checksumDigit = 10 - (sum % 10);
        }
        return checksumDigit;
    }

    private static int computeSum(int[] digits) {
        int result = 0;
        int len = digits.length;
        int[] reverse = new int[digits.length];
        for (int i = 0; i < len; i++) {
            reverse[len - 1 - i] = digits[i];
        }
        for (int i = 0; i < len; i++) {
            if (i % 2 == 0) {
                // Rang impair
                result += reverse[i];
            } else {
                // Rang pair
                result += EVEN_VALUES[reverse[i]];
            }
        }
        return result;
    }
}
